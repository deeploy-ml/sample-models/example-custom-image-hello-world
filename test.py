# Script to test your image locally

import requests

deployment_url = "http://localhost:8080/v1/models/custom-sample:predict"

request = {
    "instances": ["Deeploy"] 
}

response = requests.post(deployment_url, json=request)

print(response.json())