# Use the official lightweight Python image.
# https://hub.docker.com/_/python
FROM python:3.9-slim

# Set the environment variables for app configuration
ENV APP_HOME=/app
WORKDIR $APP_HOME
ENV APP_USER=appuser
ENV APP_PORT=8080

# Create a non-root user for the application
RUN useradd --no-log-init -r -d $APP_HOME $APP_USER

# Install production dependencies by copying the requirements file and installing the dependencies
COPY requirements.txt ./
RUN pip install --no-cache-dir -r ./requirements.txt

# Copy application code into the container
COPY app.py ./

# Switch to the non-root user for security and best practices
USER ${APP_USER}

# Run the web service on container startup. Here we use the gunicorn
# webserver, with one worker process and 8 threads.
# For environments with multiple CPU cores, increase the number of workers
# to be equal to the cores available.
ENTRYPOINT ["sh", "-c", "gunicorn -b 0.0.0.0:${APP_PORT} -w 1 -t 8 app:app"]
