import os

from flask import Flask, request

app = Flask(__name__)

@app.route('/v1/models/custom-sample:predict', methods=['POST'])
def hello_world():
    greeting_target = request.get_json()
    greeting_target = greeting_target['instances'][0]
    return { "predictions": [ 'Hello {}!'.format(greeting_target) ] }

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))